"use client";

import {
  QueryClientProvider,
  QueryClient,
  QueryCache,
} from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { useState } from "react";
// import {
//   Provider as JotaiProvider,
//   useAtom,
// } from "@/utils/functions/client-packages";

function Providers({ children }: React.PropsWithChildren) {
  const router = useRouter();
  const [client] = useState(
    () =>
      new QueryClient({
        queryCache: new QueryCache({
          onError: (error: any) => {
            // console.log("errorrrrrrrrrr", error);
            // if (["ERR_NETWORK"].includes(error.code!)) setNoInternet(true);
            // if (error?.response?.status === 406) {
            //   router.replace("/");
            //   setNoInternet(true);
            // }
            // if (["ERR_BAD_REQUEST"].includes(error.code)) router.reload();
            if (error?.response?.status === 401) {
              // deleteTokens();
              // // if (err.response?.config?.url === "/users/refresh-token" || err.config?.url === "/users/refresh-token")
              // setSignedIn(false);
              // router.replace(PagesPaths.signin);
            }
          },
        }),
        defaultOptions: {
          queries: {
            staleTime: Infinity,
            refetchOnReconnect: true,
            // retry: (failureCount, error: AxiosError) => {
            //   // console.log("failureCount, error", failureCount, error);
            //   // if (error.response?.status === 406) {
            //   //   router.replace("/");
            //   //   setShowVerify(true);
            //   // }
            //   if (error.response?.status === 401) return true;
            //   return error.response?.status === 406 || failureCount > 3 ? false : true;
            // },
          },
        },
      })
  );

  return <QueryClientProvider client={client}>{children}</QueryClientProvider>;
}

export default Providers;
