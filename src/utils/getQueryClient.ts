import { QueryClient } from "@tanstack/query-core";
import { QueryCache } from "@tanstack/react-query";
import { cache } from "react";

const getQueryClient = cache(
  () =>
    new QueryClient({
      queryCache: new QueryCache({
        onError: (error: any) => {
          console.log("server errorrrrrrrrrr");
          // if (["ERR_NETWORK"].includes(error.code!)) setNoInternet(true);
          // if (error?.response?.status === 406) {
          //   router.replace("/");
          //   setNoInternet(true);
          // }
          // if (["ERR_BAD_REQUEST"].includes(error.code)) router.reload();
          if (error?.response?.status === 401) {
            // console.log("-----", error, "-----------");
            // cookies().set(CookieKeys.access_token, "");
            // if (err.response?.config?.url === "/users/refresh-token" || err.config?.url === "/users/refresh-token")
            //   setSignedIn(false);
            //   router.replace(pagesPaths.signin);
          }
        },
      }),
    })
);
export default getQueryClient;
