import ApiService from "@/api/baseApi/api-service";
import { Config } from "../../config";
import { IGetAirportsResult } from "./interfaces/airport-interfaces";
// import {
//   IGetProductDetails,
//   IGetProductDetailsParams,
// } from "./interfaces/api-product-details-interface";
// import {
//   IGetProductResult,
//   IProductsGetAllParams,
//   IProductsSearchParams,
//   IProductsSearchResult,
// } from "./interfaces/api-product-interfaces";

const URLs = {
  search: "/airports?keyword=",
};
class ApiAirports extends ApiService {
  constructor(config: Config) {
    super({ ...config });
  }
  searchProducts = (keyword: string): Promise<IGetAirportsResult> =>
    this.get(URLs.search + keyword, {});
}
export default ApiAirports;
