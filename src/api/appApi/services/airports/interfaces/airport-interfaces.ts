export interface IAirPort {
  city: string;
  city_ar: string;
  country: string;
  country_ar: string;
  iata: string;
  id: string;
  name: string;
  name_ar: string;
}
export interface IGetAirportsResult {
  data: IAirPort[];
}
