import { useQuery } from "@tanstack/react-query";
import { apiConfig } from "../../config";
import ApiAirports from "./airports-api";

export const useAirport = () => new ApiAirports(apiConfig());

export const useSearchAirportsQuery = (keyword: string) => {
  const product = useAirport();
  return useQuery({
    queryKey: ["airports", keyword],
    queryFn: () => product.searchProducts(keyword),
    refetchOnWindowFocus: true,
    enabled: !!keyword.trim(),
  });
};
