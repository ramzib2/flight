"use client";

import { PropsWithChildren } from "react";

interface Props {
  secondary?: boolean;
  onClick: () => void;
}
const BtnHeader: React.FC<PropsWithChildren<Props>> = ({
  secondary,
  onClick,
  children,
}) => {
  return (
    <button
      className={`header-btn ${secondary ? "secondary" : "primary"}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default BtnHeader;
