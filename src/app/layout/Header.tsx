"use client";

import Image from "next/image";
import BtnHeader from "./BtnHeader";
import Link from "next/link";

const Header = () => {
  return (
    <>
      <div style={{ marginBottom: 64 }} />
      <nav
        className="pc-header nav-bar w-100"
        style={{ position: "fixed", top: 0 }}
      >
        <Link href="/">
          <Image
            src="/imgs/logo.webp"
            alt="RS"
            height={32}
            width={154}
            priority
          />
        </Link>
        <div style={{ marginInlineStart: "auto" }} />
        <BtnHeader onClick={() => {}}>الشركات</BtnHeader>
        <BtnHeader secondary onClick={() => {}}>
          تسجيل الدخــول
        </BtnHeader>
      </nav>
    </>
  );
};

export default Header;
