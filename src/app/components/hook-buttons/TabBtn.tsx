import { PropsWithChildren } from "react";

interface Props {
  rounded: boolean;
  isSelected: boolean;
  onClick: () => void;
}
const TabBtn: React.FC<PropsWithChildren<Props>> = ({
  children,
  onClick,
  isSelected,
  rounded,
}) => {
  return (
    <button
      className={`w-100 tab-btn ${isSelected ? "selected" : "not-selected"} ${
        rounded ? "rounded" : ""
      }`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default TabBtn;
