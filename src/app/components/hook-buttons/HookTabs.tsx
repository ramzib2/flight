import { Controller, FieldValues, UseFormReturn } from "react-hook-form";
import TabBtn from "./TabBtn";
import { Col, Row } from "react-bootstrap";
interface Props {
  form: UseFormReturn<FieldValues, any>;
  fieldName: string;
  captions: { key: string; value: string }[];
  rounded?: boolean;
}

const HookTabs: React.FC<Props> = ({ form, fieldName, captions, rounded }) => {
  return (
    <Controller
      control={form.control}
      name={fieldName}
      render={({ field }) => (
        <Row className="m-0">
          {captions.map((caption, index) => (
            <Col key={caption.key} className="p-0">
              <TabBtn
                rounded={rounded ?? false}
                isSelected={field.value === caption.key}
                onClick={() => field.onChange(caption.key)}
              >
                {caption.value}
              </TabBtn>
            </Col>
          ))}
        </Row>
      )}
    />
  );
};

export default HookTabs;
