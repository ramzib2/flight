interface Props {
  errorMsg: string;
}

const InputErrorMsg: React.FC<Props> = ({ errorMsg }) => {
  return <div className="auth-err">{errorMsg}</div>;
};

export default InputErrorMsg;
