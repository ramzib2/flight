/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from "react";
import { Controller, FieldValues, UseFormReturn } from "react-hook-form";
import { DropdownButton, DropdownItem, Row, Spinner } from "react-bootstrap";
import SearchDropdownTitle from "./SearchDropdownTitle";
import { useSearchAirportsQuery } from "@/api/appApi/services/airports";
import { IAirPort } from "@/api/appApi/services/airports/interfaces/airport-interfaces";
import InputErrorMsg from "./InputErrorMsg";

interface Props {
  form: UseFormReturn<FieldValues, any>;
  fieldName: string;
}
const this_field_is_required = "هذا الحقل مطلوب";
const CustomDropdown: React.FC<Props> = ({ form, fieldName }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const selectedId = form.watch(fieldName);
  const getOptions = useSearchAirportsQuery(searchTerm);
  let options: IAirPort[];
  let title: string;

  options = getOptions?.data?.data ?? [];
  title =
    selectedId === "-1"
      ? "المدينة أو المطار"
      : options.find((option) => option.id == selectedId)?.name_ar ?? "";
  const initValue = { key: -1, value: "" };
  useEffect(() => {
    if (!getOptions?.isFetched) return;
    if (
      !getOptions?.data?.data?.map((op) => op.id)?.includes(selectedId) &&
      options?.length > 1
    ) {
      setTimeout(() => form.setValue(fieldName, "-1"));
    }
  }, [getOptions?.isFetched]);
  useEffect(() => {
    if (!searchTerm) setTimeout(() => form.setValue(fieldName, "-1"));
  }, [searchTerm]);

  const isSelectedRef = useRef(false);
  const optionsToRender = options ?? [];
  return (
    <div className={`with-search-dropdown sort-dropdown w-100`}>
      <Controller
        control={form.control}
        name={fieldName}
        rules={{
          validate: (value) => (value === "-1" ? this_field_is_required : true),
        }}
        render={({ field }) => (
          <DropdownButton
            variant="outline"
            onToggle={() => {
              if (isSelectedRef.current) {
                isSelectedRef.current = false;
                return;
              }
              if (searchTerm !== "") {
                const selectedOption = options?.find(
                  (op) => op.name_ar === searchTerm
                );
                if (!selectedOption) {
                  setTimeout(() => form.setValue(fieldName, initValue?.key));
                  setSearchTerm(initValue?.value);
                }
              }
            }}
            title={
              <SearchDropdownTitle
                placeholder={title}
                searchTerm={searchTerm!}
                setSearchTerm={setSearchTerm}
              />
            }
          >
            {getOptions.isLoading ? (
              <div className="text-center">
                <Spinner animation="grow" variant="dark" />
              </div>
            ) : (
              optionsToRender.map((option, index) => (
                <DropdownItem
                  active={option?.iata === selectedId}
                  key={option.id + index}
                  onClick={() => {
                    isSelectedRef.current = true;
                    setSearchTerm(option.name_ar);
                    field.onChange(option.iata);
                  }}
                >
                  <Row className="m-0">{option?.name_ar}</Row>
                </DropdownItem>
              ))
            )}
          </DropdownButton>
        )}
      />
      <InputErrorMsg
        errorMsg={form.formState.errors[fieldName]?.message as string}
      />
    </div>
  );
};

export default CustomDropdown;
