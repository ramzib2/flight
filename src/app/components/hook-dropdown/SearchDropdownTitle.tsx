import { Col, Row } from "react-bootstrap";
import { useRef } from "react";
import SvgClearText from "./search-icons/SvgClearText";

interface Props {
  placeholder: string;
  searchTerm: string;
  setSearchTerm: (val: string) => void;
}
const SearchDropdownTitle: React.FC<Props> = ({
  placeholder,
  searchTerm,
  setSearchTerm,
}) => {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <Row className="m-0 align-items-center" style={{ gap: 8 }}>
      <Col className="p-0">
        <input
          placeholder={placeholder}
          ref={ref}
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </Col>
      {searchTerm && (
        <Col className="p-8 col-auto">
          <SvgClearText
            onClick={() => {
              setSearchTerm("");
              ref.current?.focus();
            }}
          />
        </Col>
      )}
    </Row>
  );
};

export default SearchDropdownTitle;
