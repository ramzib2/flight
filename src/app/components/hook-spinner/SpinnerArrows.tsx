import SvgSpinnerArrow from "./SvgSpinnerArrow";
interface Props {
  onClickUp: () => void;
  onClickDown: () => void;
  downDisabled: boolean;
}

const SpinnerArrows: React.FC<Props> = ({
  onClickDown,
  onClickUp,
  downDisabled,
}) => {
  return (
    <div>
      <div>
        <SvgSpinnerArrow up onClick={onClickUp} />
      </div>
      <div>
        <SvgSpinnerArrow onClick={onClickDown} disabled={downDisabled} />
      </div>
    </div>
  );
};

export default SpinnerArrows;
