import { Col, Row } from "react-bootstrap";
import SpinnerArrows from "./SpinnerArrows";
interface Props {
  onClickUp: () => void;
  onClickDown: () => void;
  count: number;
}

const Spinner: React.FC<Props> = ({ onClickDown, onClickUp, count }) => {
  return (
    <Row className="m-0 align-items-center" style={{ gap: 16 }}>
      <Col className="p-0 col-auto">
        <SpinnerArrows
          onClickUp={onClickUp}
          onClickDown={onClickDown}
          downDisabled={count === 0}
        />
      </Col>
      <Col className="p-0" style={{ fontSize: 32 }}>
        {count}
      </Col>
    </Row>
  );
};

export default Spinner;
