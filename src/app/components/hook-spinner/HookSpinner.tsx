import Spinner from "./Spinner";
import { Controller, FieldValues, UseFormReturn } from "react-hook-form";
interface Props {
  form: UseFormReturn<FieldValues, any>;
  fieldName: string;
  title: string;
}

const HookSpinner: React.FC<Props> = ({ form, fieldName, title }) => {
  return (
    <div style={{ background: "white", padding: 8 }}>
      <div style={{ textAlign: "center" }} className="primary-caption">
        {title}
      </div>
      <Controller
        control={form.control}
        name={fieldName}
        render={({ field }) => (
          <Spinner
            onClickDown={() =>
              field.value > 0 && field.onChange(field.value - 1)
            }
            onClickUp={() => field.onChange(field.value + 1)}
            count={field.value}
          />
        )}
      />
    </div>
  );
};

export default HookSpinner;
