interface Props {
  next?: boolean;
  onClick: () => void;
  disabled?: boolean;
}
const SvgNextPrevMonth: React.FC<Props> = ({
  onClick,
  next = false,
  disabled = false,
}) => {
  const scaleSign = next ? -1 : 1;
  const scaleVal = 1;
  return (
    <svg
      onClick={disabled ? undefined : onClick}
      style={{
        transform: `scale(${scaleSign * scaleVal})`,
        cursor: disabled ? "not-allowed" : "pointer",
      }}
      width="25"
      height="24"
      viewBox="0 0 25 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_17009_67062)">
        <path
          d="M16.4844 3.19218C16.2766 3.19837 16.0793 3.28524 15.9344 3.43437L7.93442 11.4344C7.78444 11.5844 7.7002 11.7879 7.7002 12C7.7002 12.2121 7.78444 12.4156 7.93442 12.5656L15.9344 20.5656C16.0081 20.6424 16.0964 20.7037 16.1941 20.7459C16.2918 20.7882 16.397 20.8105 16.5034 20.8115C16.6098 20.8126 16.7154 20.7925 16.814 20.7522C16.9125 20.712 17.002 20.6525 17.0773 20.5772C17.1526 20.502 17.2121 20.4125 17.2523 20.3139C17.2925 20.2154 17.3127 20.1098 17.3116 20.0034C17.3105 19.8969 17.2882 19.7918 17.246 19.6941C17.2037 19.5964 17.1424 19.5081 17.0657 19.4344L9.63129 12L17.0657 4.56562C17.1813 4.45325 17.2602 4.30858 17.2922 4.15056C17.3242 3.99254 17.3076 3.82856 17.2448 3.68009C17.182 3.53162 17.0758 3.4056 16.9401 3.31853C16.8044 3.23147 16.6456 3.18743 16.4844 3.19218Z"
          fill={`${disabled ? "gray" : "currentColor"}`}
        />
      </g>
      <defs>
        <clipPath id="clip0_17009_67062">
          <rect
            width="24"
            height="24"
            fill="white"
            transform="translate(0.5)"
          />
        </clipPath>
      </defs>
    </svg>
  );
};

export default SvgNextPrevMonth;
