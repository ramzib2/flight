import moment from "moment/moment";
import SvgNextPrevMonth from "./SvgNextPrevMonth";

interface Props {
  dateObject: moment.Moment;
  setDateObject: (dateObject: moment.Moment) => void;
  enabledPeriod?: {
    from: moment.Moment;
    to: moment.Moment;
  };
}

const CalendarHeader: React.FC<Props> = ({ dateObject, setDateObject, enabledPeriod = null }) => {
  let disabledPrev = false;
  let disabledNext = false;
  if (!enabledPeriod) disabledPrev = dateObject.isBefore(moment());
  else {
    disabledPrev = dateObject.isBefore(enabledPeriod.from);
    disabledNext = dateObject.isSameOrAfter(enabledPeriod.to, "month");
  }
  const year = dateObject.locale("en").format("Y");
  const month = dateObject.locale(moment.locale()).format("MMMM");
  const isRTL = moment.locale() === "ar";
  return (
    <div className="calendar-header" dir={`ltr`}>
      <SvgNextPrevMonth
        disabled={isRTL ? disabledNext : disabledPrev}
        onClick={() => {
          isRTL
            ? setDateObject(moment(dateObject).add(1, "month"))
            : setDateObject(moment(dateObject).subtract(1, "month"));
        }}
      />
      {`${month}  ${year}`}
      <SvgNextPrevMonth
        disabled={isRTL ? disabledPrev : disabledNext}
        next
        onClick={() => {
          isRTL
            ? setDateObject(moment(dateObject).subtract(1, "month"))
            : setDateObject(moment(dateObject).add(1, "month"));
        }}
      />
    </div>
  );
};

export default CalendarHeader;
