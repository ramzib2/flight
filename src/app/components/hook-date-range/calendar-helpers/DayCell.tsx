export enum SlotStatus {
  disabled,
  available,
  booked,
}

interface Props {
  blank?: boolean;
  day?: number;
  today?: boolean;
  onClick?: () => void;
  selected?: boolean;
  status?: SlotStatus;
}

const DayCell: React.FC<Props> = ({
  status,
  blank,
  day,
  today,
  onClick,
  selected,
}) => {
  if (blank) return null;
  if (status === SlotStatus.disabled)
    return (
      <div className={`day not-available`}>
        {today && <div className="today" />}
        {day}
      </div>
    );
  const booked = status === SlotStatus.booked;
  return (
    <div
      onClick={onClick}
      className={`day ${booked ? "booked" : ""} ${selected ? "selected" : ""}`}
    >
      {today && <div className="today" />}
      {day}
    </div>
  );
};

export default DayCell;
