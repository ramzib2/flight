import moment from "moment/moment";

const WeekDays = () => {
  const weekdayshort = moment.locale() === "ar" ? moment.weekdaysShort(true) : moment.weekdaysShort(true);
  return (
    <tr>
      {weekdayshort.map((day) => (
        <th key={day}>{day}</th>
      ))}
    </tr>
  );
};

export default WeekDays;
