import moment from "moment/moment";
import SvgClearText from "../../hook-dropdown/search-icons/SvgClearText";
import SvgCircle from "./SvgCircle";

interface Props {
  date: moment.Moment;
  isReturn: boolean;
  onClearClick?: () => void;
}
const DateDropdowTitle: React.FC<Props> = ({
  date,
  isReturn,
  onClearClick,
}) => {
  const header = (
    <div className="primary-caption">
      {isReturn ? (
        !date ? (
          <>
            <SvgCircle />
            تاريخ العودة
          </>
        ) : (
          "تاريخ العودة"
        )
      ) : (
        "تاريخ المغادرة"
      )}
    </div>
  );
  return (
    <div>
      <div className="text-center">{header}</div>
      {isReturn && date ? (
        <div style={{ position: "absolute", top: 8, left: 8 }}>
          <SvgClearText onClick={onClearClick!} />
        </div>
      ) : null}
      <div className="d-flex justify-content-between">
        <div className="p-0 day col-auto">{date?.date()}</div>
        <div className="">
          <div>{date?.locale("ar").format("MMMM")}</div>
          <div>{date?.locale("en").format("YYYY")}</div>
        </div>
      </div>
    </div>
  );
};

export default DateDropdowTitle;
