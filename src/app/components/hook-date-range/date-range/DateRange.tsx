import DateDropdown from "./DateDropdown";
import moment from "moment/moment";
import { Controller, FieldValues, UseFormReturn } from "react-hook-form";
import { Col, Row } from "react-bootstrap";
const calendarRange = {
  from: moment(), //.subtract(10, "year"),
  to: moment().add(1, "year"),
};

interface Props {
  form: UseFormReturn<FieldValues, any>;
  fieldStartDate: string;
  fieldEndDate: string;
}
const this_field_is_required = "هذا الحقل مطلوب";

const DateRange: React.FC<Props> = ({ form, fieldStartDate, fieldEndDate }) => {
  const firstDate = form.watch(fieldStartDate);
  const fromDateDropdown = (
    <Controller
      control={form.control}
      name={fieldStartDate}
      rules={{
        validate: (value) => (value === "-1" ? this_field_is_required : true),
      }}
      render={({ field }) => (
        <DateDropdown
          selectedDate={field.value}
          setSelectedDate={(date) => {
            field.onChange(date);
          }}
          enabledPeriod={calendarRange}
          canDeleted={false}
        />
      )}
    />
  );
  const toDateDropdown = (
    <Controller
      control={form.control}
      name={fieldEndDate}
      rules={{
        validate: (value) => (value === "-1" ? this_field_is_required : true),
      }}
      render={({ field }) => (
        <DateDropdown
          selectedDate={field.value}
          setSelectedDate={(date) => {
            field.onChange(date);
          }}
          enabledPeriod={{
            from: firstDate?.clone(),
            to: calendarRange.to,
          }}
          canDeleted
          onClearClick={() => field.onChange(null)}
        />
      )}
    />
  );
  return (
    <Row className="m-0" style={{ gap: 8 }}>
      <Col className="p-0">{fromDateDropdown}</Col>
      <Col className="p-0">{toDateDropdown}</Col>
    </Row>
  );
};

export default DateRange;
