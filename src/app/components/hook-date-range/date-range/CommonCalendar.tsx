import { Fragment, useState } from "react";
import moment from "moment/moment";
import "moment/locale/ar";
import DayCell, { SlotStatus } from "../calendar-helpers/DayCell";
import CalendarHeader from "../calendar-helpers/calender-header/CalendarHeader";
import WeekDays from "../calendar-helpers/WeekDays";

interface Props {
  selectedDate: moment.Moment;
  setSelectedDate: (date: moment.Moment) => void;
  enabledPeriod?: {
    from: moment.Moment;
    to: moment.Moment;
  };
}

const CommonCalendar: React.FC<Props> = ({
  selectedDate,
  setSelectedDate,
  enabledPeriod = null,
}) => {
  const [dateObject, setDateObject] = useState(moment());
  moment.updateLocale("en", {
    week: {
      dow: 6, // First day of week is Saturday
      doy: 12, // First week of year must contain 1 January (7 + 6 - 1)
    },
  });
  moment.locale("ar");

  const firstDayOfMonth = moment(dateObject).startOf("month").get("day") + 1; // Day of week 0..6

  const getFullDate = (d: number) => moment(dateObject).set("date", d);
  const isToday = (d: number) => moment(getFullDate(d)).isSame(moment(), "day");
  const isDateSelected = (d: number) =>
    moment(getFullDate(d)).isSame(selectedDate, "day");
  const getDateStatus = (d: number) => {
    let disabled: boolean;
    const fullDate = getFullDate(d);
    if (!enabledPeriod) disabled = fullDate.isBefore(moment(), "day");
    else
      disabled =
        fullDate.isBefore(enabledPeriod.from, "day") ||
        fullDate.isAfter(enabledPeriod.to, "day");
    return disabled ? SlotStatus.disabled : SlotStatus.available;
  };

  const blankCells = (num: number, uniquKey: string) => {
    let blanks = [];
    for (let i = 0; i < num; i++) {
      blanks.push(
        <td key={i + uniquKey}>
          <DayCell blank />
        </td>
      );
    }
    return blanks;
  };

  const startBlanks = blankCells(firstDayOfMonth, "start");
  let daysInCurrMonth = [];

  for (let d = 1; d <= dateObject.daysInMonth(); d++) {
    daysInCurrMonth.push(
      <td key={d}>
        <DayCell
          day={d}
          today={isToday(d)}
          selected={isDateSelected(d)}
          onClick={() => setSelectedDate(getFullDate(d))}
          status={getDateStatus(d)}
        />
      </td>
    );
  }
  const rem = (startBlanks.length + daysInCurrMonth.length) % 7;
  const endBlanks = rem !== 0 ? blankCells(7 - rem, "end") : [];
  let rows: any = [];
  let row: any = [];
  const totalSlots = [...startBlanks, ...daysInCurrMonth, ...endBlanks];

  totalSlots.forEach((slot, i) => {
    row.push(slot);
    if ((i + 1) % 7 === 0) {
      rows.push(row);
      row = [];
    }
  });

  const currCalendarPage = rows.map((w: any, index: number) => {
    return (
      <Fragment key={index}>
        <tr>{w}</tr>
      </Fragment>
    );
  });
  return (
    <>
      <div className="common-calendar">
        <CalendarHeader
          dateObject={dateObject}
          setDateObject={setDateObject}
          enabledPeriod={enabledPeriod!}
        />
        <table>
          <thead>
            <WeekDays />
          </thead>
          <tbody>{currCalendarPage}</tbody>
        </table>
      </div>
    </>
  );
};
export default CommonCalendar;
