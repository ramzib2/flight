import CommonCalendar from "./CommonCalendar";
import { useState } from "react";
import moment from "moment/moment";
import { DropdownButton } from "react-bootstrap";
import DateDropdowTitle from "./DateDropdowTitle";
interface Props {
  selectedDate: moment.Moment;
  setSelectedDate: (date: moment.Moment) => void;
  enabledPeriod?: {
    from: moment.Moment;
    to: moment.Moment;
  };
  canDeleted: boolean;
  onClearClick?: () => void;
}

const DateDropdown: React.FC<Props> = ({
  selectedDate,
  setSelectedDate,
  enabledPeriod = null,
  canDeleted,
  onClearClick,
}) => {
  moment.locale("ar");
  const [show, setSshow] = useState(false);
  return (
    <div className="date-dropdown">
      <DropdownButton
        variant="outline"
        show={show}
        onToggle={() => setSshow(!show)}
        title={
          <DateDropdowTitle
            date={selectedDate!}
            isReturn={canDeleted}
            onClearClick={onClearClick}
          />
        }
      >
        <div className="pt-3">
          <CommonCalendar
            enabledPeriod={enabledPeriod!}
            selectedDate={selectedDate}
            setSelectedDate={(date) => {
              setSelectedDate(date);
              setSshow(false);
            }}
          />
        </div>
      </DropdownButton>
    </div>
  );
};

export default DateDropdown;
