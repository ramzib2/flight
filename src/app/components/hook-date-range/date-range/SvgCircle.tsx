const SvgCircle = () => {
  return (
    <svg height="32" width="32" xmlns="http://www.w3.org/2000/svg">
      <circle r="10" cx="16" cy="16" fill="#106cf9" />
      <text fill={"white"} stroke="white" x="16" y="20" textAnchor="middle">
        +
      </text>
    </svg>
  );
};

export default SvgCircle;
