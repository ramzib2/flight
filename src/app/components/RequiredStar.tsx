interface Props {
  title: string;
  isRequired: boolean;
  style?: React.CSSProperties;
}
const RequiredStar: React.FC<Props> = ({ title, isRequired, style }) => {
  return (
    <div className="builder-item-label" style={{ ...style }}>
      {title}
      {isRequired && <strong className="text-danger"> * </strong>}
    </div>
  );
};

export default RequiredStar;
