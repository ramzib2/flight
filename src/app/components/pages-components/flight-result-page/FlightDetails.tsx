import { Col, Row } from "react-bootstrap";
import SvgChair from "./icons/SvgChair";
import SvgHorArrow from "./icons/SvgHorArrow";
import SvgSeat from "./icons/SvgSeat";
import ChooseButton from "./ChooseButton";

const FlightDetails = () => {
  return (
    <div>
      <Row className="m-0">
        <Col className="p-0">
          <Row
            className="m-0  align-items-center flight-box"
            style={{
              gap: 48,
            }}
          >
            <Col className="p-0">
              <div>
                <SvgSeat />
              </div>
            </Col>
            <Col className="p-0">
              <Row
                className="m-0 align-items-center flex-nowrap"
                style={{ gap: 24 }}
              >
                <Col className="p-0">
                  <div>00:10</div>
                  <div>DUH</div>
                </Col>

                <Col className="p-0">
                  <SvgHorArrow />
                </Col>
                <Col className="p-0">
                  <div>01:10</div>
                  <div>DUH</div>
                </Col>
              </Row>
            </Col>
            <Col className="p-0 col-auto">
              <SvgChair />
            </Col>
          </Row>
        </Col>
        <Col className="col-auto flight-box">
          <div style={{ textAlign: "center" }}>1273 ريال</div>
          <ChooseButton />
        </Col>
      </Row>
    </div>
  );
};

export default FlightDetails;
