const SvgSeat = () => {
  return (
    <div
      className="d-flex align-items-center justify-content-center"
      style={{
        width: "3.5em",
        height: "3.5em",
        border: "2px solid rgb(255, 140, 32)",
        borderRadius: "50%",
      }}
    >
      <svg
        stroke="currentColor"
        fill="currentColor"
        strokeWidth="0"
        version="1"
        viewBox="0 0 48 48"
        enableBackground="new 0 0 48 48"
        className="fs-4"
        height="1.5em"
        width="1.5em"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path fill="#BF360C" d="M41.2,5h-7.3L32,43h11L41.2,5z"></path>
        <path fill="#E64A19" d="M33,23h-4v-6l-12,6v-6L5,23v20h28V23z"></path>
        <rect x="9" y="27" fill="#FFC107" width="4" height="4"></rect>
        <rect x="17" y="27" fill="#FFC107" width="4" height="4"></rect>
        <rect x="25" y="27" fill="#FFC107" width="4" height="4"></rect>
        <rect x="9" y="35" fill="#FFC107" width="4" height="4"></rect>
        <rect x="17" y="35" fill="#FFC107" width="4" height="4"></rect>
        <rect x="25" y="35" fill="#FFC107" width="4" height="4"></rect>
      </svg>
    </div>
  );
};

export default SvgSeat;
