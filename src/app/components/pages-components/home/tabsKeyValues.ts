export const travelTypes = [
  { key: "trips", value: "رحلات" },
  { key: "hotels", value: "فنادق" },
];
export const flightTypes = [
  { key: "oneWay", value: "ذهاب فقط" },
  { key: "roundTrip", value: "ذهاب وعودة" },
  { key: "manyTrips", value: "رحلات متعددة" },
];
export const flightLevels = [
  { key: "first", value: "الأولى" },
  { key: "business", value: "الأعمال" },
  { key: "economy", value: "السياحية" },
];
