"use client";

import { useForm } from "react-hook-form";
import CustomDropdown from "../../hook-dropdown/CustomDropdown";
import HookSpinner from "../../hook-spinner/HookSpinner";
import { Col, Row } from "react-bootstrap";
import HookTabs from "../../hook-buttons/HookTabs";
import DateRange from "../../hook-date-range/date-range/DateRange";
import moment from "moment/moment";
import SvgPlainUp from "../../hook-dropdown/search-icons/SvgPlainUp";
import SvgPlainDown from "../../hook-dropdown/search-icons/SvgPlainDown";
import SvgUpDown from "../../hook-dropdown/search-icons/SvgUpDown";
import { flightLevels, flightTypes, travelTypes } from "./tabsKeyValues";
import { useRouter } from "next/navigation";
const SelectYourTravel = () => {
  const router = useRouter();
  const hookForm = useForm<any>({
    defaultValues: {
      travelType: travelTypes[0].key,
      flightType: flightTypes[0].key,
      originLocationIata: -1,
      destinationIata: -1,
      infant: 0,
      child: 0,
      adults: 0,
      date: moment(),
      r_date: null,
      flightLevel: flightLevels[0].key,
    },
  });
  const onSubmit = (data: any) => {
    const dataToSend = {
      ...data,
      date: data.date?.locale("en").format("YYYY-MM-DD"),
      r_date: data.r_date?.format("YYYY-MM-DD"),
    };
    const params = [];
    for (const [key, value] of Object.entries(dataToSend)) {
      params.push(`${key}=${value}`);
    }
    router.push("flight-result?" + params.join("&"));
  };
  return (
    <form
      onSubmit={hookForm.handleSubmit(onSubmit)}
      className="travel-form-container"
    >
      <div className="tabs-container">
        <HookTabs
          rounded
          form={hookForm}
          fieldName="travelType"
          captions={travelTypes}
        />
      </div>
      <div className="tabs-container">
        <HookTabs
          rounded
          form={hookForm}
          fieldName="flightType"
          captions={flightTypes}
        />
      </div>
      <div>
        <div
          className="dropdown-container origin"
          style={{ position: "relative" }}
        >
          <div className="title">
            <SvgPlainUp />
            من
          </div>
          <CustomDropdown form={hookForm} fieldName="originLocationIata" />
          <div style={{ position: "absolute", bottom: -14, right: 16 }}>
            <SvgUpDown />
          </div>
        </div>
        <div className="dropdown-container destination">
          <div className="title">
            <SvgPlainDown />
            إلى
          </div>
          <CustomDropdown form={hookForm} fieldName="destinationIata" />
        </div>
      </div>
      <Row className="m-0" style={{ gap: 8 }}>
        <Col className="p-0">
          <HookSpinner form={hookForm} fieldName="infant" title="رضيع" />
        </Col>
        <Col className="p-0">
          <HookSpinner form={hookForm} fieldName="child" title="طفل" />
        </Col>
        <Col className="p-0">
          <HookSpinner form={hookForm} fieldName="adults" title="بالغ" />
        </Col>
      </Row>
      <DateRange form={hookForm} fieldStartDate="date" fieldEndDate="r_date" />
      <HookTabs
        form={hookForm}
        fieldName="flightLevel"
        captions={flightLevels}
      />
      <button className="submit-btn w-100">ابحث عن رحلة</button>
    </form>
  );
};

export default SelectYourTravel;
