import FlightDetails from "../components/pages-components/flight-result-page/FlightDetails";

export default function Page({ searchParams }: { searchParams: any }) {
  console.log(searchParams);
  return (
    <div
      className="row"
      style={{ padding: 24, maxWidth: 1252, margin: "auto", gap: 24 }}
    >
      <div className="col p-0 col-auto border">
        <div style={{ width: 300 }}>Options</div>
      </div>
      <div className="col">
        <div className="d-flex flex-column" style={{ gap: 24 }}>
          <FlightDetails />
          <FlightDetails />
          <FlightDetails />
          <FlightDetails />
        </div>
      </div>
    </div>
  );
}
