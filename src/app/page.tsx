import { Col, Row } from "react-bootstrap";
import SelectYourTravel from "./components/pages-components/home/SelectYourTravel";

export default function Home() {
  return (
    <div style={{ padding: 24, maxWidth: 1252, margin: "auto" }}>
      <Row className="m-0" style={{ gap: 72 }}>
        <Col className="p-0 col-auto">
          <SelectYourTravel />
        </Col>
        <Col className="d-none d-lg-block">
          <img src="/imgs/airplane.webp" alt="" width="100%" />
        </Col>
      </Row>
    </div>
  );
}
